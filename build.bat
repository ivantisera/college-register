echo off
echo College Register build
    del node_modules\*.* /s /f /q
    call npm install -g gulp
    call npm install -g http-server
    call npm install -g json-server
    call npm install 
    start npm start
    start gulp all:watch
    start json-server --watch db.json 
PAUSE

