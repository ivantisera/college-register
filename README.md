##Aplicación de prueba
College Register.

Se pidió un formulario multipasos de venta que guarde los datos que el usuario va cargando en localStorage por si se abandona el circuito: debe volver al paso donde estaba y con los campos completados.

Se utiliza AngularJS para el frontend y se usa json-server como backend de prueba. 
En cada paso se llama un servicio para guardar lo que el cliente carga (solo escribe en un json que queda en el front a fin de hacer las pruebas)

Para levantar el proyecto se necesita Node 10 y se debe ejecutar build.bat que hace las instalaciones correspondientes y levanta el proyecto.
Para hacerlo a mano, se debe ejecutar:

npm install -g gulp
npm install -g http-server
npm install -g json-server
npm install 
npm start
gulp all:watch
json-server --watch db.json 
