(function () {
    'use strict';
   
    angular.module('collegeRegister.controllers', []);
    angular.module('collegeRegister.services', []);
    angular.module('collegeRegister.directives', []);
    angular.module('collegeRegister.directives.conn-error', []);
    angular.module('collegeRegister.directives.step-bar', []);
    angular.module('collegeRegister.directives.validate-me', []);

})();
