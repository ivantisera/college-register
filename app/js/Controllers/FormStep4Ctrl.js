(function () {
    'use strict';
    angular
        .module('collegeRegister.controllers')
        .controller('FormStep4Ctrl', FormStep4Ctrl);

    function FormStep4Ctrl($scope, $state) {
        var vm = this;

        vm.goStep1 = goStep1;

        init();

        function init() {
            $scope.basectrl.setCurrentStep(4);

        }

        function goStep1() {
            $state.go('main-form.step1');
        }
       
    };
})();
