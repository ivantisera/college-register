(function () {
    'use strict';
    angular
        .module('collegeRegister.controllers')
        .controller('FormStep2Ctrl', FormStep2Ctrl);

    function FormStep2Ctrl($scope, $state, formService, formFactory, localStorageFactory, $q) {
        var vm = this;

        var operations = {
            formStep2: {
                facSelected: null,
                carreerSelected: null,
                locationSelected: null,
                isFormSubmitted: false
            },
            hasError: false,
            loading: false,
            facs: null,
            carreers: null,
            locations: null
        };
        vm.operations = operations;

        vm.goStep1 = goStep1;
        vm.loadCarreers = loadCarreers;
        vm.disableCareers = disableCareers;
        vm.onFieldsBlur = onFieldsBlur;
        vm.goStep3 = goStep3;

        init();

        /**
         * Inicializa la pantalla.
         * @returns {Void}
         */
        function init() {
            $scope.basectrl.setCurrentStep(2);
            loadSelectBoxes();
        }

        /**
         * Carga los select llamando a los servicios correspondientes.
         * Al finalizar con la carga invoca la precarga con datos que el usuario haya seleccionado anteriormente.
         * 
         * @returns {Void}
         */
        function loadSelectBoxes() {
            operations.loading = true;
            $q.all([formService.getCareers(), formService.getLocations()]).then(function (responses) {
                operations.facs = responses[0].result;
                operations.locations = responses[1].result;
                loadPrechargedData();
            }).catch(function (error) {
                operations.hasError = true;
            }).finally(function () {
                operations.loading = false;
            });
        }

        /**
         * Precarga de datos de existir.
         * 
         * @returns {Void}
         */
        function loadPrechargedData() {
            var oldData = localStorageFactory.get("step2");

            if (oldData !== null) {
                operations.formStep2.facSelected = oldData.facSelected;
                if (!angular.isUndefinedOrNull(operations.formStep2.facSelected)) {
                    loadCarreers();
                    operations.formStep2.carreerSelected = oldData.carreerSelected;
                }
                operations.formStep2.locationSelected = oldData.locationSelected;

            }
        }

        /**
         * Carga de select de carreras. Depende de la selección del select "Facultades"
         * 
         * @returns {Void}
         */
        function loadCarreers() {
            var facid = operations.formStep2.facSelected;

            operations.loading = true;
            operations.formStep2.carreerSelected = null;
            angular.forEach(operations.facs, function (item) {

                if (facid == item.id) {
                    operations.carreers = item.carreras;
                    operations.loading = false;
                }
            });
        }

        /**
         * Deshabilita select de carreras cuando el de Facultades no está seleccionado.
         * 
         * @returns {Boolean} True para disable
         */
        function disableCareers() {
            return angular.isUndefinedOrNull(operations.formStep2.facSelected);
        }

        /**
         * Llamada desde el blur de cada item, actualiza el localStorage.
         * 
         * @returns {Void}
         */
        function onFieldsBlur() {
            updateLS();
        }

        /**
         * Actualiza localStorage.
         * 
         * @returns {Void}
         */
        function updateLS() {
            localStorageFactory.set("step2", operations.formStep2);
        }

        /**
         * Redirige al paso anterior. Llamada desde el botón VOLVER.
         * 
         * @returns {Void}
         */
        function goStep1() {
            $state.go('main-form.step1');
        }

        /**
         * Chequea que el form sea válido.
         * Lee el id del formulario y envía al backend el avance.
         * Redirige al próximo paso
         * 
         * @param {Object} form 
         * @returns {Void}
         */
        function goStep3(form) {
            if (form.$valid) {
                var fId = formFactory.getFormId();
                var params = {
                    "formId": fId,
                    "step": "2"
                };
                operations.loading = true;
                operations.hasError = false;

                operations.formStep2.isFormSubmitted = true;

                formService.saveAdvance(params).then(function (response) {
                    updateLS();
                    $state.go("main-form.step3");
                }).catch(function () {
                    operations.hasError = true;
                }).finally(function () {
                    operations.loading = false;
                });
            } else {
                return;
            }

        }
    };
})();
