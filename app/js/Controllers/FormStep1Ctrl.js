(function () {
    'use strict';
    angular
        .module('collegeRegister.controllers')
        .controller('FormStep1Ctrl', FormStep1Ctrl);

    function FormStep1Ctrl($scope, localStorageFactory, formService, formFactory, $state) {
        var vm = this;

        var operations = {
            formStep1: {
                name: null,
                lastName: null,
                nationality: null,
                docType: null,
                docNumber: null,
                mail: null,
                isFormSubmitted: false
            },
            hasError: false,
            loading: false,
            dniSelect: []
        };

        vm.operations = operations;
        vm.onFieldsBlur = onFieldsBlur;
        vm.goStep2 = goStep2;

        init();

        /**
         * Inicializa la pantalla. Setea el paso actual e invoca a la función que carga los tipo de documento.
         */
        function init() {
            $scope.basectrl.setCurrentStep(1);
            loadDniList();
        }

        /**
         * Lee localStorage y si quedó guardado algún dato de una visita previa lo carga.
         * @returns {Void}
         */
        function loadPrechargedData() {
            var oldData = localStorageFactory.get("step1");
            if (oldData !== null) {
                operations.formStep1.name = oldData.name;
                operations.formStep1.lastName = oldData.lastName;
                operations.formStep1.nationality = oldData.nationality;
                operations.formStep1.docType = oldData.docType;
                operations.formStep1.docNumber = oldData.docNumber;
                operations.formStep1.mail = oldData.mail;
                operations.formStep1.formId = oldData.formId;
            }
        }

        /**
         * Carga el select de tipo de documento e invoca a la carga de datos precargados en localStorage, de haberlos.
         * @returns {Void}
         */
        function loadDniList() {
            operations.hasError = false;
            operations.loading = true;
            formService.getDNI().then(function (response) {
                operations.dniSelect = response.result;
                loadPrechargedData();
            }).catch(function (error) {
                operations.hasError = true;
            }).finally(function () {
                operations.loading = false;
            });

        }

        /**
         * Llamada desde el blur de cada campo, actualiza el localStorage de manera de que siempre esté este con lo último ingresado.
         */
        function onFieldsBlur() {
            updateLS();
        }

        /**
         * Actualiza el localStorage 
         * @returns {Void}
         */
        function updateLS() {
            localStorageFactory.set("step1", operations.formStep1);
        }

        /**
         * Chequea que el form sea válido, actualiza el localStorage e invoca a la función que guarda el avance en el backend 
         * @param {Object} form formulario
         * @returns {void}
         */
        function goStep2(form) {
       
            if (form.$valid) {
                operations.formStep1.isFormSubmitted = true;
                updateLS();
                saveAndRedirect();
            } else {
                return;
            }
        }

        /**
         * Guarda en el backend que el usuario completó el step1.
         * Actualiza localstorage.
         * Redirige al siguiente paso.
         * 
         * @returns {Void}
         */
        function saveAndRedirect() {
            var fId = formFactory.getFormId();
            var params = {
                "formId": fId,
                "step": "1"
            };

            operations.loading = true;
            operations.hasError = false;

            if (angular.isUndefinedOrNull(fId)) {
                fId = formFactory.createFormId(operations.formStep1.docNumber);
                formFactory.setFormId(fId);
            }

            formService.saveAdvance(params).then(function (response) {
                updateLS();
                $state.go("main-form.step2");
            }).catch(function () {
                operations.hasError = true;
            }).finally(function () {
                operations.loading = false;
            });
        }

    };
})();
