(function () {
    'use strict';
    angular
        .module('collegeRegister.controllers')
        .controller('FormStep3Ctrl', FormStep3Ctrl);

    function FormStep3Ctrl($scope, $state, formService, formFactory, localStorageFactory) {
        var vm = this;

        var operations = {
            formStep3: {
                message: null,
                newsletter: false,
            },
            hasError: false,
            loading: false,
        };
        vm.operations = operations;

        vm.onFieldsBlur = onFieldsBlur;
        vm.goStep2 = goStep2;
        vm.confirm = confirm;

        init();

        function init() {
            $scope.basectrl.setCurrentStep(3);

        }


        function onFieldsBlur() {
            updateLS();
        }

        function updateLS() {
            localStorageFactory.set("step3", operations.formStep3);
        }

        function goStep2() {
            $state.go('main-form.step2');
        }

        function confirm(form) {

            if (form.$valid) {
                operations.formStep3.isFormSubmitted = true;
                updateLS();
                saveData();
            } else {
                return;
            }

        }


        function saveData() {
            var form = {
                "formcontent": {
                    "step1": localStorageFactory.get("step1"),
                    "step2": localStorageFactory.get("step2"),
                    "step3": localStorageFactory.get("step3"),
                    "formId": formFactory.getFormId()
                }
            };

            var params = { "form": form };

            operations.loading = true;

            formService.sendForm(params).then(function (response) {
                localStorageFactory.clear();
                $state.go("main-form.step4");
            }).catch(function (error) {
                operations.hasError = true;
            }).finally(function () {
                operations.loading = false;
            });
        }
    };
})();
