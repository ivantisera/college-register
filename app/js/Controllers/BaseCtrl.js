(function () {
    'use strict';
    angular
        .module('collegeRegister.controllers')
        .controller('BaseCtrl', BaseCtrl);

    function BaseCtrl($scope, $state, formFactory) {
        var vm = this;

        var steps = {
            config: {
                1: '1. Tus Datos Personales',
                2: '2. Carrera',
                3: '3. Comentarios',
                4: '4. ¡Listo!'
            },
            currentStep: null
        }

        vm.steps = steps;
        vm.setCurrentStep = setCurrentStep;
        init();

        function init() {
            goCurrentStep();
        }

        function goCurrentStep() {
            var storedCurrent = formFactory.getLastStep();
            $state.go(storedCurrent);
        }

        function setCurrentStep(step) {
            if (step > 0) {
                steps.currentStep = step;
               // operations.displayPBar = true;
            }
        }


    };
})();
