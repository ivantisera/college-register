
(function () {
  'use strict';

  angular.module('collegeRegister', [
    // 'ngRoute',
    'ui.router',
    'ngResource',
    'collegeRegister.version',
    'collegeRegister.controllers',
    'collegeRegister.services',
    'collegeRegister.directives',
    'collegeRegister.directives.conn-error',
    'collegeRegister.directives.step-bar',
    'collegeRegister.directives.validate-me',
    'collegeRegister.config'
  ]);

})();


