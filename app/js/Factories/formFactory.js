(function () {
    'use strict';
    angular
        .module('collegeRegister.services')
        .factory('formFactory', formFactory);

    function formFactory(localStorageFactory) {
        var vm = this;

        vm.getLastStep = getLastStep;
        vm.createFormId = createFormId;
        vm.setFormId = setFormId;
        vm.getFormId = getFormId;

        return vm;

        /**
         * Lee localstorage y define a qué paso del formulario se debe acceder. 
         * 
         * @returns {String} El nombre del estado al que redirige. Posibles: main-form.step1|main-form.step2|main-form.step1
         */
        function getLastStep() {
            var last = "main-form.step3";
            if (isStepFull("step1") && !isStepFull("step2")) {
                last = "main-form.step2";
            } else if (!isStepFull("step1")) {
                last = "main-form.step1";
            }
            return last;
        }

        /**
         * Define si el step pasado por parametro está completo 
         * 
         * @param {String} step Valores posibles: step1|step2|step3 
         * @returns {Boolean}
         */
        function isStepFull(step) {
            return ((localStorageFactory.exists(step)) && (localStorageFactory.get(step).isFormSubmitted == true));
        }


        /**
         * Genera un id para el formulario. Idealmente esta tarea debería hacerla el backend al realizar el primer envío de información
         * pero dadas las limitaciones del actual sistema de pruebas no puedo agregar lógica al backend.
         * 
         * @param {Int} dni Dni de quién hace la consulta
         * @returns {String}
         */
        function createFormId(dni) {
            return dni + "-" + (Math.floor(Math.random() * 100) + 1);
        }

        /**
         * Getter de formId
         * @returns {String|null}
         */
        function getFormId() {
            var formId = null;
            if (localStorageFactory.exists("formId")){
                formId = localStorageFactory.get("formId");
            }
            return formId;
        }

        /**
         * Setea el valor del formId. Podría hacerse desde la función que lo crea, pero suponiendo un futuro release con backend con mayor funcionalidad
         * se setearía este valor al ser retornado del servicio.
         * Guarda el valor en el localStorage. 
         * @param {String} val 
         */
        function setFormId(val) {
            localStorageFactory.set("formId", val);
        }
    };
})();
