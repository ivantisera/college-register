(function () {
    'use strict';
    angular
        .module('collegeRegister.services')
        .factory('localStorageFactory', localStorageFactory);

    function localStorageFactory() {
        var vm = this;

        vm.get = get;
        vm.set = set;
        vm.clear = clear;
        vm.remove = remove;
        vm.exists = exists

        return vm;


        function set(key, data) {
            localStorage.setItem(key, JSON.stringify(data));
        }

        function get(key) {
            var value = null;
            if (localStorage.getItem(key) != null) {
                value = JSON.parse(localStorage.getItem(key));
            }
            return value;
        }

        function clear() {
            localStorage.clear();
        }

        function remove(key) {
            localStorage.removeItem(key);
        }

        function exists(key) {
            return !angular.isUndefinedOrNull(get(key));
        }
    };
})();
