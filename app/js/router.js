
(function () {
    'use strict';

    angular
        .module('collegeRegister')
        .config(['$stateProvider', '$urlRouterProvider', formRouter]);

    function formRouter($stateProvider, $urlRouterProvider) {
        $stateProvider
             .state('main-form', {
                 abstract: true,
                 url: '/main-form',
                 templateUrl: '/views/base.html',
                 controller: 'BaseCtrl',
                 controllerAs: 'basectrl'
             })
            .state('main-form.step1', {
                url: '/step1',
                name: "step1",
                templateUrl: '/views/step1.html',
                controller: 'FormStep1Ctrl',
                controllerAs: 'step1'
            })
            .state('main-form.step2', {
                url: '/step2',
                name: "step2",
                templateUrl: '/views/step2.html',
                controller: 'FormStep2Ctrl',
                controllerAs: 'step2'
            })
            .state('main-form.step3', {
                url: '/step3',
                name: "step3",
                templateUrl: '/views/step3.html',
                controller: 'FormStep3Ctrl',
                controllerAs: 'step3'
            })
             .state('main-form.step4', {
                 url: '/step4',
                 name: "step4",
                 templateUrl: '/views/step4.html',
                 controller: 'FormStep4Ctrl',
                 controllerAs: 'step4'
             });

            $urlRouterProvider.otherwise("/main-form/step1");

    };


})();
