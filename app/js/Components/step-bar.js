(function () {
    'use strict';
    angular
        .module('collegeRegister.directives.step-bar')
        .directive('stepBar', stepBar);
    function stepBar() {
        return {
            restrict: 'A',
            templateUrl: '../js/Components/step-bar.html',
            replace: false,
            scope: {
                steps: '='
            },
            link: function (scope, element) {
                scope.stepsLenght = Object.keys(scope.steps.config).length;

                scope.getTitle = function () {
                    return scope.steps.config[scope.steps.currentStep];
                };

                scope.$watch('steps.currentStep', function (newVal) {
                    if (newVal == 0) {
                        return scope.steps.currentStep = 1;
                    } else if (newVal > scope.stepsLenght) {
                        return scope.steps.currentStep = scope.stepsLenght;
                    }
                });
            }

        }
    }
})();
