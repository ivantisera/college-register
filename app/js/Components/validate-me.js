(function () {
    'use strict';
    angular
        .module('collegeRegister.directives.validate-me')
        .directive('validateMe', validateMe);

    function validateMe($compile) {
        return {
            restrict: 'AE',
            require: ['^form', 'ngModel'],
            link: function (scope, element, attrs, ngModel) {

                /**
                 * aplica la validación si se submitea el form padre
                 */
                scope.$watch(function () { return ngModel[0].$submitted }, function (a, b, input) {
                    validate();
                }, true);

                /**
                 * aplica validacion en cada cambio del model del campo
                 */
                scope.$watch(attrs.ngModel, function (a, b, input) {
                    validate();
                }, true);

                /**
                 * Patrones de validacion
                 * @type {Array}
                 */
                var validations = [];
                validations["onlyLetters"] = /^([A-Za-zÑñáéíóúÁÉÍÓÚ\s])*$/;
                validations["onlyNumbersNotZero"] = /^([1-9][0-9]*)$/;
                validations["mail"] = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                /**
                 * Mensajes de error
                 * @type {Array}
                 */
                var errorMsgs = [];
                errorMsgs["onlyLetters"] = "Solo admite letras";
                errorMsgs["onlyNumbersNotZero"] = "Solo números que no comiencen en cero";
                errorMsgs["mail"] = "Correo electrónico no válido";

                /**
                 * Ejecuta la validacion del campo.
                 * Se activa solo si el campo está $dirty o el form $submitted
                 * 
                 * @returns {Void}
                 */
                function validate() {
                    var item = ngModel[0][attrs.name];
                    var itemValue = item.$modelValue;
                    var isFormSubmitted = ngModel[0].$submitted;
                    var isThisDirty = item.$dirty;


                    if (isThisDirty || isFormSubmitted) {
                        cleanErrors();
                        if (angular.isUndefinedOrNull(itemValue) || itemValue == "") {
                            showError("Campo requerido");
                        }
                        else {
                            if (!angular.isUndefinedOrNull(attrs.validation)) {
                                testIt(itemValue);
                            }
                        }
                    }
                }

                /**
                 * Ejecuta la validación para expresiones regulares. Se llama desde validate()
                 * 
                 * @param {String|Int} itemValue 
                 * @returns {Void}
                 */
                function testIt(itemValue) {
                    if (!validations[attrs.validation].test(itemValue)) {
                        showError(errorMsgs[attrs.validation]);
                    }
                }

                /**
                 * Limpia los errores.
                 * 
                 * @returns {Void}
                 */
                function cleanErrors() {
                    ngModel[0][attrs.name].$setValidity('custom', true);
                    var myElement = angular.element(document.querySelector('#' + attrs.name));
                    myElement.remove();
                }

                /**
                 * Crea el div con el error y lo attachea al item.
                 * Esta parte de la directiva debería mejorarse y no editar el DOM, 
                 * sino tener su propio template y mostrar o no con un ng-if. 
                 * Se hizo así a los términos de la prueba.
                 * 
                 * @param {String} errorMsg Mensaje de error a mostrar.
                 */
                function showError(errorMsg) {
                    cleanErrors();
                    ngModel[0][attrs.name].$setValidity('custom', false);
                    var content = '<div id="' + attrs.name + '"class="errorMsg"><span>' + errorMsg + '</span></div>';
                    element.after($compile(content)(scope));
                }

            }
        }
    }
})();