(function () {
    'use strict';
    angular.isUndefinedOrNull = function (obj) {
        return angular.isUndefined(obj) || obj === null;
    };
})();
