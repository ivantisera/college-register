(function () {
    'use strict';
    angular
        .module('collegeRegister.services')
        .service('formService', formService);

    function formService($http, $resource, API, POST_HEADERS) {
        return {
            getDNI: getDNI,
            getCareers: getCareers,
            getLocations: getLocations,
            sendForm: sendForm,
            saveAdvance: saveAdvance
        };

        function getDNI() {
            return $resource(API + "/dni").get().$promise;

        }

        function getCareers() {
            return $resource(API + "/facultades").get().$promise;
        }

        function getLocations() {
            return $resource(API + "/sedes").get().$promise;
        }

        function sendForm(params) {
            return $http({ method: 'POST', url: API + '/enviarformulario', headers: POST_HEADERS, data: params });
        }

        function saveAdvance(params){
            return $http({ method: 'POST', url: API + '/estadocarga', headers: POST_HEADERS, data: params });
        }
    }


})();
