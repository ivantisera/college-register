'use strict';

angular.module('collegeRegister.version', [
  'collegeRegister.version.interpolate-filter',
  'collegeRegister.version.version-directive'
])

.value('version', '0.1');
