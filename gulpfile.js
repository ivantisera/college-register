
var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var ngconfig = require('gulp-ng-config');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');
var inject = require('gulp-inject');
var taskName = process.argv.slice(2);

var paths = {
    sass: ['./app/scss/**/*.scss'],
    js: ['./app/js/**/*.js']
};

gulp.task('index', function () {
    var target = gulp.src('./app/index.html');
    var sources = gulp.src(['./app/dist/*.js', './app/components/version/*.js','./app/dist/*.css'], { read: false, relative: true });

    return target.pipe(inject(sources, { relative: true }))
        .pipe(gulp.dest('./app'));
});

gulp.task('config-desa', function () {
    gulp
        .src('./app-config-desa.json')
        .pipe(ngconfig('collegeRegister.config'))
        .pipe(concat('config.js'))
        .pipe(gulp.dest('./app/js/'));
})

gulp.task('config-prod', function () {
    gulp
        .src('./app-config-prod.json')
        .pipe(ngconfig('collegeRegister.config'))
        .pipe(concat('config.js'))
        .pipe(gulp.dest('./app/js/'));
})

gulp.task('default', ['sass']);

gulp.task('js', function () {

    gulp
        .src(paths.js)
        .pipe(sourcemaps.init())
        .pipe(concat('bundle.js'))
        .pipe(sourcemaps.write())
        .pipe(gulpif(taskName == 'prod', uglify()))
        .pipe(gulp.dest('./app/dist/'));

});

gulp.task('sass', function () {
    gulp
        .src(paths.sass)
        .pipe(sourcemaps.init())
        .pipe(concat('app.css'))
        .pipe(sourcemaps.write())
        .pipe(sass().on("error", sass.logError))
        .pipe(gulpif(taskName == 'prod', cleanCSS()))
        .pipe(gulp.dest('./app/dist/'));

});

gulp.task('prod', ['config-prod', 'js', 'sass'])


gulp.task('watch:js', ['js'], function () {
    gulp.watch(paths.js, ['js']);
});

gulp.task('watch:sass', ['sass'], function () {
    gulp.watch(paths.sass, ['sass']);
});

gulp.task('all:watch', ['js', 'sass'], function () {

    gulp.watch(paths.js, ['js']);
    gulp.watch(paths.sass, ['sass']);

});
